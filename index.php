<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title> CosmicDotExe </title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=VT323">
    <link rel="stylesheet" href="assets/css/index.css">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Josefin+Sans" />
  </head>
  <body>
    <script src="assets/js/client.js"></script>
  <div class="video-background">
    <div class="video-foreground">
      <!--<iframe src="https://www.youtube.com/embed/hSlb1ezRqfA?controls=0&showinfo=0&rel=0&autoplay=1&loop=1&playlist=W0LHTWG-UmQ" frameborder="0" allowfullscreen></iframe>-->
      <iframe src="https://www.youtube.com/embed/FG0fTKAqZ5g?controls=0&showinfo=0&rel=0&autoplay=1&loop=1&playlist=W0LHTWG-UmQ" frameborder="0" id="videobg" allowfullscreen></iframe>
    </div>
  </div>


  <div id="vidtop-content">
    <canvas id="canvas"></canvas>
    <div class="vid-info">
	  <h1 id="titlemain">C O S M I C  . E X E</h1>
      <div class="undertitle">
        <p class="superp" id="ip"><?php echo $_SERVER["REMOTE_ADDR"]; ?> - CMD</p>
        <input type="text" onkeypress="return login(event)" id="userInput" value="user$ " autofocus onfocus="var temp_value=this.value; this.value=''; this.value=temp_value">
        <!--<a onclick="login()" class="superbutton">enter</a>-->
      </div>

  </div>
  </div>
    <script src="assets/js/dots.js"></script>
  </body>
</html>
